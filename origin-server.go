/*
Go origin server made to test client proxy and server proxy
*/

package main

import (
	"fmt"
	"log"
	"net"
)

func main() {
	//Server variables
	config := "10.0.0.170:9003"

	//listening on port 9004 at host localhost
	ln, err := net.Listen("tcp", config)
	if err != nil {
		panic(err)
	}

	//callback to close the server in case of error
	defer ln.Close()
	fmt.Println("Server started at ", config, "\nWaiting for requests...\n")
	//Forever loop to listen and wait for connection requests
	for {
		//Accept the connection
		conn, err := ln.Accept()
		handleErr(err)
		//spawn routine to handle concurrent serve requests
		go serveRequest(conn)
	}
}

//function to handle errors
func handleErr(err error) {
	if err != nil {
		log.Fatal(err)
		panic(err)
	}
}

//function to handle serve request between origin server and server proxy
func serveRequest(conn net.Conn) {
	//Callback to end the connection when error occurs
	defer conn.Close()
	fmt.Println("\nNew routine started")

	//buffer for reading data
	buff := make([]byte, 1024)

	//ack or it can be substituted by the response of the server with data
	for {
		//read from the connection
		read_err := read_conn(conn, buff)
		if read_err == "Connection closed" {
			fmt.Println(read_err + " from proxy")
		}
		//reply with the message
		_, err := conn.Write([]byte("Origin server: Message received\n"))
		if err != nil {
			fmt.Println("Connection closed from the proxy")
			break
		}
	}

	fmt.Println("\nRoutine completed")
}
//Function to completely read the message from proxy
func read_conn(conn net.Conn, buff []byte) (read_err string) {

	read_err = ""
	//Loop to read the message
	for {
		_, err := conn.Read(buff)
		//handle read error
		if err != nil {
			conn.Close()
			read_err = "Connection closed"
			break
		}
		//check if buffer is full. If yes then print the current value and read again from connection.
		if buff[1023] != 0 {
			fmt.Print(string(buff))
			buff = make([]byte, 1024)
			continue
		} else {
			//if buffer not full the print the current value and break from the loop
			fmt.Println(string(buff))
			break
		}
	}
	return
}
