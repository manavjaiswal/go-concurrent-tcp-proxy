/*
generic tcp proxy
*/

package main

import (
	"fmt"
	"net"
	"os"
	"sync"
)

func main() {

	//receive the config from command-line
	config := os.Args[1:]
	var (
		ADD1, ADD2 string
	)
	if(config[0] == "s") {
		ADD1 = "10.0.0.170:9002"
		ADD2 = "10.0.0.170:9003"
	} else if(config[0] == "c") {
		ADD2 = "10.0.0.170:9002"
		ADD1 = "10.0.0.170:9001"
	}

		fmt.Println("Starting proxy at "+ ADD1)
		ln, err := net.Listen("tcp", ADD1)
		if err != nil {
			panic(err)
		}

	fmt.Println("Proxy started")
	//callback to close the server in case of error
	defer ln.Close()

	//Forever loop to listen and wait for connection requests
	for {
		//Accept the connection
		conn1, err := ln.Accept()
		if (err != nil) {
			break
		}
		//if server proxy then dial to the server address provided as the third command-line argument

		conn2, err := net.Dial("tcp", ADD2)
		if err != nil {
			conn1.Close()
			break
		}
			//spawn a routine to handle both the sockets
		go handle_session(conn1, conn2)
	}
}

func handle_session(conn1, conn2 net.Conn) {
	defer conn1.Close()
	defer conn2.Close()
	//wait group object

	var wg sync.WaitGroup
	//should wait until either of the socket closes
	wg.Add(1)
	//routine to handle the read from client and consequent write to server
	go func() {
		fmt.Println("Routine spawned to read from client")
		buff := make([]byte, 1024)
		for {
			//flushing the buffer after every iteration
			buff = make([]byte, 1024)
			//read from client
			_, err := conn1.Read(buff)
			if (err != nil) {
				fmt.Println("Connection closed from client")
        conn2.Close()
				break
			}
			fmt.Println("From client: " + string(buff))
			//write to the server proxy
			_, err = conn2.Write(buff)
			if err != nil {
				fmt.Println("Connection closed with server")
        conn1.Close()
				break
			}

		}
		fmt.Println("Client routine completed")
	}()
	//routine to handle read from server proxy and consequent write to client
	go func() {
		fmt.Println("Routine spawned to read from server")
		buff := make([]byte, 1024)
		for {
			//flushing the buffer before iteration
			buff = make([]byte, 1024)
			//reading from the server proxy
			_, err := conn2.Read(buff)
			if err != nil {
				fmt.Println("Connection closed with server")
        conn1.Close()
				break
			}
			fmt.Println("From server: " + string(buff))
			//writing to the client
			_, err = conn1.Write(buff)
			if err != nil {
				fmt.Println("Connection closed with client")
        conn2.Close()
				break
			}
		}
		fmt.Println("Server routine completed")
	}()
	//wait for either of the routine to complete
	wg.Wait()
	fmt.Println("Closing sockets and ending session")
}
