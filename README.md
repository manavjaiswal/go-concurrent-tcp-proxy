******************************************************************************************************************
******************************************************************************************************************
Go concurrent TCP client proxy and server proxy

This repository is to demonstrate the concurrent TCP client and server proxy.

How to run:
1) Run origin-server.go by:

    go run origin-proxy.go

2) Run tcp-proxy.go in a new terminal with command-line arguments for specifying it's a reverse proxy by "s" as first argument:

    go tcp-proxy.go s

3) Run tcp-proxy.go in a new terminal with command-line arguments for specifying it's a forward proxy by "c" as first argument:

    go tcp-proxy.go c  

4) Establish a connection from client side using telnet to client proxy

5) Messages typed in the client console will travel through client proxy --> server proxy --> origin server and the reply from the origin server, that is "Origin server: Message received", is hard-coded in origin-server.go, will be visible on the client console following the reverse route.

6) Trigger close connection from client console, it will trigger closing of sockets from client till the origin server.

******************************************************************************************************************
******************************************************************************************************************
